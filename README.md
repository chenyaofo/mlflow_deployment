# MLFlow Deployment


## Build docker image

```
docker build -t mlflow-runtime:v1 .
```

## Example to run


```
docker run -it --rm --network host  -v /home/chenyaofo/workspace/mlflow_deployment:/workspace  mlflow-runtime:v1 bash
```

When moving into docker container, use 

```
mlserver start .
```