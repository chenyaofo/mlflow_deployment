import torch
from torchvision.models import resnet50

import mlflow.pytorch
from mlflow.models.signature import infer_signature


model = resnet50(pretrained=True)

X = torch.rand(1,3,224,224)
with torch.no_grad():
    y = model(X)

signature = infer_signature(X.numpy(), y.numpy())

mlflow.pytorch.log_model(model, "resnet50.mlflow", signature=signature)

